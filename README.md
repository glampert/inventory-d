
## Inventory-d

Auto organizer for RPG-like game inventories, written using the D Programming Language.
It features a brute force approach to organize the inventory, as well as a more sophisticated
(but less efficient!) *Genetic Algorithms* approach.

----

Sample screenshot -- Left: randomized, Right: auto-organized.

![inventory-d](https://bytebucket.org/glampert/inventory-d/raw/d5926596cc62de7a6a0928361c7b571ad2b58b67/inventoryd.png "Inventory Organizer")

----

This project's source code is released under the [MIT License](http://opensource.org/licenses/MIT).

