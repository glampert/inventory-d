
# OSX Makefile for inventoryd.
# Not tested on Linux, but should work as well.
# Requires Derelict3 and GLFW.

PROJECT_NAME   = inventoryd
SOURCE_FILES   = Main.d Inventory.d GA.d GLStuff.d
COMPILER       = dmd
COMPILER_FLAGS = -debug -g # => DEBUG MODE!
INCLUDE_PATHS  = Derelict3/import
LIB_FILES      = Derelict3/lib/dmd/libDerelictUtil.a Derelict3/lib/dmd/libDerelictGL3.a Derelict3/lib/dmd/libDerelictGLFW3.a

$(PROJECT_NAME): $(SOURCE_FILES)
	$(COMPILER) $(COMPILER_FLAGS) -of$(PROJECT_NAME) -I$(INCLUDE_PATHS) -L$(LIB_FILES) $(SOURCE_FILES)

clean:
	rm -f *.o $(PROJECT_NAME)

.PHONY: clean

